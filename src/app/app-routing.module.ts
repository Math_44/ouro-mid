import { PagProdutosComponent } from './components/pag-produtos/pag-img/pag-produtos.component';
import { PagProdutosVideosComponent } from './components/pag-produtos-videos/pag-produtos-videos.component';
import { PaginicialProdutosComponent } from './components/paginicial-produtos/paginicial-produtos.component';
import { QuemSomosComponent } from './components/quem-somos/quem-somos.component';
import { HomeComponent } from './components/home/home.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: "",
    component: HomeComponent
  },
  {
    path: "produtos",
    component: PaginicialProdutosComponent
  },

  {

    path: "imagens",
    component: PagProdutosComponent


  },

  {

    path: "videos",
    component: PagProdutosVideosComponent

  },

  {
    path: "quemSomos",
    component: QuemSomosComponent

  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
