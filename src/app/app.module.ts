import { PromocoesComponent } from './components/promocoes/promocoes-img/promocoes.component';
import { PagProdutosComponent } from './components/pag-produtos/pag-img/pag-produtos.component';

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavComponent } from './components/template/nav/nav.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FooterComponent } from './components/template/footer/footer.component';
import { HeaderComponent } from './components/template/header/header.component';
import { HomeComponent } from './components/home/home.component';
import { SlidesComponent } from './components/slides/slides.component';

import { QuemSomosComponent } from './components/quem-somos/quem-somos.component';
import { ImgsComponent } from './components/slides/imgs/imgs.component';
import { CreateServicesComponent } from './components/create-services/create-services.component';
import { PlanosComponent } from './components/planos/planos.component';
import { PagProdutosVideosComponent } from './components/pag-produtos-videos/pag-produtos-videos.component';
import { PagCartaodigitalComponent } from './components/pag-produtos/pag-cartaodigital/pag-cartaodigital.component';
import { PaginicialProdutosComponent } from './components/paginicial-produtos/paginicial-produtos.component';
import { PromocoesVideosComponent } from './components/promocoes/promocoes-videos/promocoes-videos.component';
import { WhatsappComponent } from './components/whatsapp/whatsapp.component';

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    SlidesComponent,
    PagProdutosComponent,
    QuemSomosComponent,
    ImgsComponent,
    CreateServicesComponent,
    PlanosComponent,
    PromocoesComponent,
    PagProdutosVideosComponent,
    PagCartaodigitalComponent,
    PaginicialProdutosComponent,
    PromocoesVideosComponent,
    WhatsappComponent,
    
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule
  
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
