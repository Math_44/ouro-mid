import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PromocoesVideosComponent } from './promocoes-videos.component';

describe('PromocoesVideosComponent', () => {
  let component: PromocoesVideosComponent;
  let fixture: ComponentFixture<PromocoesVideosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PromocoesVideosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PromocoesVideosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
