import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PagCartaodigitalComponent } from './pag-cartaodigital.component';

describe('PagCartaodigitalComponent', () => {
  let component: PagCartaodigitalComponent;
  let fixture: ComponentFixture<PagCartaodigitalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PagCartaodigitalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PagCartaodigitalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
