import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-pag-produtos',
  templateUrl: './pag-produtos.component.html',
  styleUrls: ['./pag-produtos.component.css']
})
export class PagProdutosComponent implements OnInit {

  constructor(private modalService: NgbModal) { }

  ngOnInit(): void {
    
  }
  
  enviar(modal){

    this.modalService.open(modal);

  }


}
