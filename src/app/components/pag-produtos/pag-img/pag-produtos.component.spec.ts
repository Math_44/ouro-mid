import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PagProdutosComponent } from './pag-produtos.component';

describe('PagProdutosComponent', () => {
  let component: PagProdutosComponent;
  let fixture: ComponentFixture<PagProdutosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PagProdutosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PagProdutosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
