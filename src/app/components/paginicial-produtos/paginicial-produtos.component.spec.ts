import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaginicialProdutosComponent } from './paginicial-produtos.component';

describe('PaginicialProdutosComponent', () => {
  let component: PaginicialProdutosComponent;
  let fixture: ComponentFixture<PaginicialProdutosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaginicialProdutosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaginicialProdutosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
