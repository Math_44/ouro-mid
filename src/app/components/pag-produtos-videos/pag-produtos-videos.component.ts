import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pag-produtos-videos',
  templateUrl: './pag-produtos-videos.component.html',
  styleUrls: ['./pag-produtos-videos.component.css']
})
export class PagProdutosVideosComponent implements OnInit {

  constructor(private modalService: NgbModal) { }

  ngOnInit(): void {
  }

  enviar(modal){

    this.modalService.open(modal);

  }

}
