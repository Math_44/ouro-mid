import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PagProdutosVideosComponent } from './pag-produtos-videos.component';

describe('PagProdutosVideosComponent', () => {
  let component: PagProdutosVideosComponent;
  let fixture: ComponentFixture<PagProdutosVideosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PagProdutosVideosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PagProdutosVideosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
